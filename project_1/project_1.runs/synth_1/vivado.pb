
|
Command: %s
53*	vivadotcl2K
7synth_design -top GameController -part xc7a100tcsg324-12default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7a100t2default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7a100t2default:defaultZ17-349h px� 
�
%s*synth2�
xStarting RTL Elaboration : Time (s): cpu = 00:00:03 ; elapsed = 00:00:02 . Memory (MB): peak = 410.301 ; gain = 100.789
2default:defaulth px� 
�
synthesizing module '%s'638*oasys2"
GameController2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
492default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter g_playerH bound to: 100 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_playerW bound to: 15 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter g_ballSize bound to: 10 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter g_ballSpeed bound to: 150 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter g_colorSpeed bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2"
clockGenerator2default:default2�
�C:/Projects/DigitalElectronics/project_1/project_1.runs/synth_1/.Xil/Vivado-24288-DELLLAPTOPMAES/realtime/clockGenerator_stub.vhdl2default:default2
52default:default2
clkgen2default:default2"
clockGenerator2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
1782default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2"
clockGenerator2default:default2�
�C:/Projects/DigitalElectronics/project_1/project_1.runs/synth_1/.Xil/Vivado-24288-DELLLAPTOPMAES/realtime/clockGenerator_stub.vhdl2default:default2
142default:default8@Z8-638h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
Random2default:default2d
PC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/Random.vhd2default:default2
82default:default2
randGen2default:default2
Random2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
1852default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
Random2default:default2f
PC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/Random.vhd2default:default2
182default:default8@Z8-638h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
Random2default:default2
12default:default2
12default:default2f
PC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/Random.vhd2default:default2
182default:default8@Z8-256h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
VPulse2default:default2d
PC:/Projects/DigitalElectronics/RedScreen/RedScreen.srcs/sources_1/new/VPulse.vhd2default:default2
52default:default2
VSync2default:default2
VPulse2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
1932default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
VPulse2default:default2f
PC:/Projects/DigitalElectronics/RedScreen/RedScreen.srcs/sources_1/new/VPulse.vhd2default:default2
292default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter g_visible bound to: 480 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter g_front bound to: 10 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter g_back bound to: 33 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter g_sync bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter g_visible_H bound to: 640 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_front_H bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter g_sync_H bound to: 96 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter g_back_H bound to: 48 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter g_visible bound to: 640 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter g_front bound to: 16 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter g_sync bound to: 96 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter g_back bound to: 33 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
HPulse2default:default2d
PC:/Projects/DigitalElectronics/RedScreen/RedScreen.srcs/sources_1/new/HPulse.vhd2default:default2
52default:default2
HS2default:default2
HPulse2default:default2f
PC:/Projects/DigitalElectronics/RedScreen/RedScreen.srcs/sources_1/new/VPulse.vhd2default:default2
552default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
HPulse2default:default2f
PC:/Projects/DigitalElectronics/RedScreen/RedScreen.srcs/sources_1/new/HPulse.vhd2default:default2
192default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter g_visible bound to: 640 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter g_front bound to: 16 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter g_sync bound to: 96 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter g_back bound to: 33 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
HPulse2default:default2
22default:default2
12default:default2f
PC:/Projects/DigitalElectronics/RedScreen/RedScreen.srcs/sources_1/new/HPulse.vhd2default:default2
192default:default8@Z8-256h px� 
�
Eport width mismatch for port '%s': port width = %s, actual width = %s549*oasys2 
Hcounter_out2default:default2
102default:default2
322default:default2f
PC:/Projects/DigitalElectronics/RedScreen/RedScreen.srcs/sources_1/new/VPulse.vhd2default:default2
502default:default8@Z8-549h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
VPulse2default:default2
32default:default2
12default:default2f
PC:/Projects/DigitalElectronics/RedScreen/RedScreen.srcs/sources_1/new/VPulse.vhd2default:default2
292default:default8@Z8-256h px� 
^
%s
*synth2F
2	Parameter g_startX bound to: 20 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_startY bound to: 190 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_height bound to: 100 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter g_width bound to: 15 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
Player2default:default2d
PC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/Player.vhd2default:default2
52default:default2
Player12default:default2
Player2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
2022default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
Player2default:default2f
PC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/Player.vhd2default:default2
282default:default8@Z8-638h px� 
^
%s
*synth2F
2	Parameter g_startX bound to: 20 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_startY bound to: 190 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_fieldH bound to: 460 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter g_screenH bound to: 480 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter g_width bound to: 15 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_height bound to: 100 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter g_speed bound to: 150 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter g_Freq bound to: 300 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
Tick2default:default2^
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
52default:default2
	moveClock2default:default2
Tick2default:default2f
PC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/Player.vhd2default:default2
432default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
Tick2default:default2`
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
112default:default8@Z8-638h px� 
]
%s
*synth2E
1	Parameter g_Freq bound to: 300 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
Tick2default:default2
42default:default2
12default:default2`
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
112default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
Player2default:default2
52default:default2
12default:default2f
PC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/Player.vhd2default:default2
282default:default8@Z8-256h px� 
�
-Port '%s' is missing in component declaration4102*oasys2
shrink2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
1242default:default8@Z8-5640h px� 
_
%s
*synth2G
3	Parameter g_startX bound to: 605 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_startY bound to: 190 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter g_ballSize bound to: 10 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_height bound to: 100 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter g_width bound to: 15 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
AI2default:default2`
LC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/AI.vhd2default:default2
52default:default2
Player22default:default2
AI2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
2112default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
AI2default:default2b
LC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/AI.vhd2default:default2
332default:default8@Z8-638h px� 
_
%s
*synth2G
3	Parameter g_startX bound to: 605 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_startY bound to: 190 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_fieldH bound to: 460 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter g_screenH bound to: 480 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter g_width bound to: 15 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_height bound to: 100 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter g_speed bound to: 150 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter g_ballSize bound to: 10 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_startX bound to: 605 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_startY bound to: 190 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_fieldH bound to: 460 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter g_screenH bound to: 480 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter g_width bound to: 15 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_height bound to: 100 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter g_speed bound to: 150 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
Player2default:default2d
PC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/Player.vhd2default:default2
52default:default2
aiPlayer2default:default2
Player2default:default2b
LC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/AI.vhd2default:default2
632default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2*
Player__parameterized12default:default2f
PC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/Player.vhd2default:default2
282default:default8@Z8-638h px� 
_
%s
*synth2G
3	Parameter g_startX bound to: 605 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_startY bound to: 190 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_fieldH bound to: 460 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter g_screenH bound to: 480 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter g_width bound to: 15 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_height bound to: 100 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter g_speed bound to: 150 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter g_Freq bound to: 300 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
Tick2default:default2^
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
52default:default2
	moveClock2default:default2
Tick2default:default2f
PC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/Player.vhd2default:default2
432default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2*
Player__parameterized12default:default2
52default:default2
12default:default2f
PC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/Player.vhd2default:default2
282default:default8@Z8-256h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2
random2default:default2b
LC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/AI.vhd2default:default2
812default:default8@Z8-614h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
AI2default:default2
62default:default2
12default:default2b
LC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/AI.vhd2default:default2
332default:default8@Z8-256h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2 
ScoreDisplay2default:default2j
VC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/ScoreDisplay.vhd2default:default2
52default:default2

ScoreBoard2default:default2 
ScoreDisplay2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
2222default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2 
ScoreDisplay2default:default2l
VC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/ScoreDisplay.vhd2default:default2
152default:default8@Z8-638h px� 
^
%s
*synth2F
2	Parameter g_Freq bound to: 1600 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
Tick2default:default2^
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
52default:default2
clk2default:default2
Tick2default:default2l
VC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/ScoreDisplay.vhd2default:default2
382default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2(
Tick__parameterized22default:default2`
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
112default:default8@Z8-638h px� 
^
%s
*synth2F
2	Parameter g_Freq bound to: 1600 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2(
Tick__parameterized22default:default2
62default:default2
12default:default2`
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
112default:default8@Z8-256h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2*
BCDTo7SegmentConverter2default:default2�
rC:/Projects/DigitalElectronics/7SegmentController/7SegmentController.srcs/sources_1/new/BCDTo7SegmentConverter.vhd2default:default2
72default:default2
	converter2default:default2*
BCDTo7SegmentConverter2default:default2l
VC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/ScoreDisplay.vhd2default:default2
422default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2*
BCDTo7SegmentConverter2default:default2�
rC:/Projects/DigitalElectronics/7SegmentController/7SegmentController.srcs/sources_1/new/BCDTo7SegmentConverter.vhd2default:default2
122default:default8@Z8-638h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2*
BCDTo7SegmentConverter2default:default2
72default:default2
12default:default2�
rC:/Projects/DigitalElectronics/7SegmentController/7SegmentController.srcs/sources_1/new/BCDTo7SegmentConverter.vhd2default:default2
122default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2 
ScoreDisplay2default:default2
82default:default2
12default:default2l
VC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/ScoreDisplay.vhd2default:default2
152default:default8@Z8-256h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
AudioDriver2default:default2a
MC:/Projects/DigitalElectronics/Audio/Audio.srcs/sources_1/new/AudioDriver.vhd2default:default2
52default:default2
au2default:default2
AudioDriver2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
2292default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
AudioDriver2default:default2c
MC:/Projects/DigitalElectronics/Audio/Audio.srcs/sources_1/new/AudioDriver.vhd2default:default2
152default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter g_outFreq bound to: 500 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter g_Freq bound to: 44100 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
Tick2default:default2^
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
52default:default2
clock2default:default2
Tick2default:default2c
MC:/Projects/DigitalElectronics/Audio/Audio.srcs/sources_1/new/AudioDriver.vhd2default:default2
312default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2(
Tick__parameterized42default:default2`
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
112default:default8@Z8-638h px� 
_
%s
*synth2G
3	Parameter g_Freq bound to: 44100 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2(
Tick__parameterized42default:default2
82default:default2
12default:default2`
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
112default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
AudioDriver2default:default2
92default:default2
12default:default2c
MC:/Projects/DigitalElectronics/Audio/Audio.srcs/sources_1/new/AudioDriver.vhd2default:default2
152default:default8@Z8-256h px� 
]
%s
*synth2E
1	Parameter g_Freq bound to: 300 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
Tick2default:default2^
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
52default:default2
genClock2default:default2
Tick2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
2362default:default8@Z8-3491h px� 
[
%s
*synth2C
/	Parameter g_Freq bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
Tick2default:default2^
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
52default:default2
genClock2default:default2
Tick2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
2362default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2(
Tick__parameterized62default:default2`
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
112default:default8@Z8-638h px� 
[
%s
*synth2C
/	Parameter g_Freq bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2(
Tick__parameterized62default:default2
92default:default2
12default:default2`
JC:/Projects/DigitalElectronics/Counter/Counter.srcs/sources_1/new/Tick.vhd2default:default2
112default:default8@Z8-256h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2
Color2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
3552default:default8@Z8-614h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2"
GameController2default:default2
102default:default2
12default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
492default:default8@Z8-256h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[31]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[30]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[29]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[28]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[27]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[26]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[25]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[24]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[23]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[22]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[21]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[20]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[19]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[18]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[17]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[16]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[15]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[14]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[13]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[12]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[11]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[10]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[9]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[8]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[7]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[6]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[5]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[4]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[3]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[2]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[1]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[0]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[31]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[30]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[29]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[28]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[27]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[26]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[25]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[24]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[23]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[22]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[21]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[20]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[19]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[18]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[17]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[16]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[15]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[14]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[13]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[12]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[11]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[10]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	shrink[9]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	shrink[8]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	shrink[7]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	shrink[6]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	shrink[5]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	shrink[4]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	shrink[3]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	shrink[2]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	shrink[1]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	shrink[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2"
GameController2default:default2
BTND2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2"
GameController2default:default2
BTNR2default:defaultZ8-3331h px� 
�
%s*synth2�
xFinished RTL Elaboration : Time (s): cpu = 00:00:03 ; elapsed = 00:00:03 . Memory (MB): peak = 466.617 ; gain = 157.105
2default:defaulth px� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:04 ; elapsed = 00:00:04 . Memory (MB): peak = 466.617 ; gain = 157.105
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:00:04 ; elapsed = 00:00:04 . Memory (MB): peak = 466.617 ; gain = 157.105
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
Loading part %s157*device2$
xc7a100tcsg324-12default:defaultZ21-403h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
>

Processing XDC Constraints
244*projectZ1-262h px� 
=
Initializing timing engine
348*projectZ1-569h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
�c:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/ip/clockGenerator/clockGenerator/clockGenerator_in_context.xdc2default:default2
clkgen	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
�c:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/ip/clockGenerator/clockGenerator/clockGenerator_in_context.xdc2default:default2
clkgen	2default:default8Z20-847h px� 
�
Parsing XDC File [%s]
179*designutils2t
^C:/Projects/DigitalElectronics/project_1/project_1.srcs/constrs_1/new/Nexys-A7-100t-Master.xdc2default:default8Z20-179h px� 
�
Finished Parsing XDC File [%s]
178*designutils2t
^C:/Projects/DigitalElectronics/project_1/project_1.srcs/constrs_1/new/Nexys-A7-100t-Master.xdc2default:default8Z20-178h px� 
�
�Implementation specific constraints were found while reading constraint file [%s]. These constraints will be ignored for synthesis but will be used in implementation. Impacted constraints are listed in the file [%s].
233*project2r
^C:/Projects/DigitalElectronics/project_1/project_1.srcs/constrs_1/new/Nexys-A7-100t-Master.xdc2default:default24
 .Xil/GameController_propImpl.xdc2default:defaultZ1-236h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0012default:default2
827.0042default:default2
0.0002default:defaultZ17-268h px� 
H
&Completed Processing XDC Constraints

245*projectZ1-263h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
827.0042default:default2
0.0002default:defaultZ17-268h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
827.0042default:default2
0.0002default:defaultZ17-268h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common24
 Constraint Validation Runtime : 2default:default2
00:00:002default:default2 
00:00:00.0072default:default2
827.0042default:default2
0.0002default:defaultZ17-268h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
~Finished Constraint Validation : Time (s): cpu = 00:00:11 ; elapsed = 00:00:12 . Memory (MB): peak = 827.004 ; gain = 517.492
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Loading part: xc7a100tcsg324-1
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:11 ; elapsed = 00:00:12 . Memory (MB): peak = 827.004 ; gain = 517.492
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Z
%s
*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:00:11 ; elapsed = 00:00:12 . Memory (MB): peak = 827.004 ; gain = 517.492
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
s
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
CLK2default:defaultZ8-5546h px� 
s
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
CLK2default:defaultZ8-5546h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2!
powerTable[3]2default:default2
22default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2!
powerTable[3]2default:default2
22default:default2
52default:defaultZ8-5544h px� 
s
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
CLK2default:defaultZ8-5546h px� 
v
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pwmOut2default:defaultZ8-5546h px� 
s
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
CLK2default:defaultZ8-5546h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
3492default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
3502default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
3512default:default8@Z8-5818h px� 
�
cNot enough pipeline registers after wide multiplier. Recommended levels of pipeline registers is %s4267*oasys2
42default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
3492default:default8@Z8-5845h px� 
�
cNot enough pipeline registers after wide multiplier. Recommended levels of pipeline registers is %s4267*oasys2
42default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
3502default:default8@Z8-5845h px� 
�
cNot enough pipeline registers after wide multiplier. Recommended levels of pipeline registers is %s4267*oasys2
42default:default2n
XC:/Projects/DigitalElectronics/project_1/project_1.srcs/sources_1/new/GameController.vhd2default:default2
3512default:default8@Z8-5845h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:11 ; elapsed = 00:00:13 . Memory (MB): peak = 827.004 ; gain = 517.492
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 18    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     31 Bit       Adders := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     21 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     15 Bit       Adders := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      4 Bit       Adders := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 2     
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 5     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               31 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               21 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               15 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                7 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                6 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 10    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     32 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   8 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   7 Input     32 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   6 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     31 Bit        Muxes := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     21 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     15 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      9 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   6 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 21    
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Hierarchical RTL Component report 
2default:defaulth p
x
� 
C
%s
*synth2+
Module GameController 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     31 Bit       Adders := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      4 Bit       Adders := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   8 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   7 Input     32 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   6 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     31 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      9 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 6     
2default:defaulth p
x
� 
;
%s
*synth2#
Module Random 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               31 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     31 Bit        Muxes := 1     
2default:defaulth p
x
� 
;
%s
*synth2#
Module HPulse 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
;
%s
*synth2#
Module VPulse 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
Module Tick 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     15 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               15 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     15 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
;
%s
*synth2#
Module Player 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 2     
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
K
%s
*synth23
Module Player__parameterized1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 2     
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
7
%s
*synth2
Module AI 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     31 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit       Adders := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     31 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
I
%s
*synth21
Module Tick__parameterized2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
K
%s
*synth23
Module BCDTo7SegmentConverter 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
A
%s
*synth2)
Module ScoreDisplay 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     31 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     15 Bit       Adders := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     32 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     31 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     15 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   6 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
I
%s
*synth21
Module Tick__parameterized4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                7 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
@
%s
*synth2(
Module AudioDriver 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                6 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
I
%s
*synth21
Module Tick__parameterized6 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     21 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               21 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     21 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2k
WPart Resources:
DSPs: 240 (col length:80)
BRAMs: 270 (col length: RAMB18 80 RAMB36 40)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
]
%s
*synth2E
1Warning: Parallel synthesis criteria is not met 
2default:defaulth p
x
� 
}
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2!
moveClock/CLK2default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2*
aiPlayer/moveClock/CLK2default:defaultZ8-5546h px� 
w
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
clk/CLK2default:defaultZ8-5546h px� 
v
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pwmOut2default:defaultZ8-5546h px� 
y
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
	clock/CLK2default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2-
genClocks[0].genClock/CLK2default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2-
genClocks[1].genClock/CLK2default:defaultZ8-5546h px� 
c
%s
*synth2K
7DSP Report: Generating DSP L2, operation Mode is: A*B.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
c
%s
*synth2K
7DSP Report: Generating DSP L2, operation Mode is: A*B.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
n
%s
*synth2V
BDSP Report: Generating DSP L2, operation Mode is: (PCIN>>17)+A*B.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
c
%s
*synth2K
7DSP Report: Generating DSP L2, operation Mode is: A*B.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
c
%s
*synth2K
7DSP Report: Generating DSP L2, operation Mode is: A*B.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
n
%s
*synth2V
BDSP Report: Generating DSP L2, operation Mode is: (PCIN>>17)+A*B.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
c
%s
*synth2K
7DSP Report: Generating DSP L2, operation Mode is: A*B.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
c
%s
*synth2K
7DSP Report: Generating DSP L2, operation Mode is: A*B.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
n
%s
*synth2V
BDSP Report: Generating DSP L2, operation Mode is: (PCIN>>17)+A*B.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
]
%s
*synth2E
1DSP Report: operator L2 is absorbed into DSP L2.
2default:defaulth p
x
� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[31]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[30]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[29]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[28]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[27]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[26]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[25]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[24]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[23]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[22]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[21]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[20]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[19]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[18]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[17]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[16]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[15]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[14]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[13]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[12]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[11]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
AI2default:default2
	ballX[10]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[9]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[8]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[7]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[6]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[5]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[4]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[3]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[2]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[1]2default:defaultZ8-3331h px� 
y
!design %s has unconnected port %s3331*oasys2
AI2default:default2
ballX[0]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[31]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
AI2default:default2

shrink[30]2default:defaultZ8-3331h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33312default:default2
1002default:defaultZ17-14h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[1][2]2default:default2
FDRE2default:default2'
ballSpeed_reg[1][3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[1][3]2default:default2
FDRE2default:default2'
ballSpeed_reg[1][4]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[1][4]2default:default2
FDRE2default:default2'
ballSpeed_reg[1][5]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[1][5]2default:default2
FDRE2default:default2'
ballSpeed_reg[1][6]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[1][6]2default:default2
FDRE2default:default2'
ballSpeed_reg[1][7]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[1][7]2default:default2
FDRE2default:default2'
ballSpeed_reg[1][8]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[1][8]2default:default2
FDRE2default:default2'
ballSpeed_reg[1][9]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[1][9]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][10]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][11]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][12]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][12]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][13]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][13]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][14]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][14]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][15]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][15]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][16]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][16]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][17]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][17]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][18]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][18]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][19]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][19]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][20]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][20]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][21]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][21]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][22]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][22]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][23]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][23]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][24]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][24]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][25]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][25]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][26]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][26]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][27]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][27]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][28]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][28]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][29]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][29]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][30]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[1][30]2default:default2
FDRE2default:default2(
ballSpeed_reg[1][31]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[0][2]2default:default2
FDRE2default:default2'
ballSpeed_reg[0][3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[0][3]2default:default2
FDRE2default:default2'
ballSpeed_reg[0][4]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[0][4]2default:default2
FDRE2default:default2'
ballSpeed_reg[0][5]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[0][5]2default:default2
FDRE2default:default2'
ballSpeed_reg[0][6]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[0][6]2default:default2
FDRE2default:default2'
ballSpeed_reg[0][7]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[0][7]2default:default2
FDRE2default:default2'
ballSpeed_reg[0][8]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[0][8]2default:default2
FDRE2default:default2'
ballSpeed_reg[0][9]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2'
ballSpeed_reg[0][9]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][10]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][11]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][12]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][12]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][13]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][13]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][14]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][14]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][15]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][15]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][16]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][16]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][17]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][17]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][18]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][18]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][19]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][19]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][20]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][20]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][21]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][21]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][22]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][22]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][23]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][23]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][24]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][24]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][25]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][25]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][26]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][26]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][27]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][27]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][28]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][28]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][29]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][29]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][30]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2(
ballSpeed_reg[0][30]2default:default2
FDRE2default:default2(
ballSpeed_reg[0][31]2default:defaultZ8-3886h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:00:27 ; elapsed = 00:00:28 . Memory (MB): peak = 900.043 ; gain = 590.531
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
_
%s*synth2G
3
DSP: Preliminary Mapping  Report (see note below)
2default:defaulth px� 
�
%s*synth2�
�+---------------+----------------+--------+--------+--------+--------+--------+------+------+------+------+-------+------+------+
2default:defaulth px� 
�
%s*synth2�
�|Module Name    | DSP Mapping    | A Size | B Size | C Size | D Size | P Size | AREG | BREG | CREG | DREG | ADREG | MREG | PREG | 
2default:defaulth px� 
�
%s*synth2�
�+---------------+----------------+--------+--------+--------+--------+--------+------+------+------+------+-------+------+------+
2default:defaulth px� 
�
%s*synth2�
�|GameController | A*B            | 15     | 15     | -      | -      | 15     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|GameController | A*B            | 18     | 18     | -      | -      | 48     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|GameController | (PCIN>>17)+A*B | 15     | 15     | -      | -      | 15     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|GameController | A*B            | 15     | 15     | -      | -      | 15     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|GameController | A*B            | 18     | 18     | -      | -      | 48     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|GameController | (PCIN>>17)+A*B | 15     | 15     | -      | -      | 15     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|GameController | A*B            | 15     | 15     | -      | -      | 15     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|GameController | A*B            | 18     | 18     | -      | -      | 48     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|GameController | (PCIN>>17)+A*B | 15     | 15     | -      | -      | 15     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�+---------------+----------------+--------+--------+--------+--------+--------+------+------+------+------+-------+------+------+

2default:defaulth px� 
�
%s*synth2�
�Note: The table above is a preliminary report that shows the DSPs inferred at the current stage of the synthesis flow. Some DSP may be reimplemented as non DSP primitives later in the synthesis flow. Multiple instantiated DSPs are reported only once.
2default:defaulth px� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
R
%s
*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
2Moved timing constraint from pin '%s' to pin '%s'
4028*oasys2"
clkgen/CLKGame2default:default2+
clkgen/bbstub_CLKGame/O2default:defaultZ8-5578h px� 
�
2Moved timing constraint from pin '%s' to pin '%s'
4028*oasys2#
clkgen/CLKPixel2default:default2,
clkgen/bbstub_CLKPixel/O2default:defaultZ8-5578h px� 
�
SMoved %s constraints on hierarchical pins to their respective driving/loading pins
4235*oasys2
22default:defaultZ8-5819h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:00:38 ; elapsed = 00:00:39 . Memory (MB): peak = 900.043 ; gain = 590.531
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
|Finished Timing Optimization : Time (s): cpu = 00:00:39 ; elapsed = 00:00:40 . Memory (MB): peak = 900.043 ; gain = 590.531
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
{Finished Technology Mapping : Time (s): cpu = 00:00:41 ; elapsed = 00:00:42 . Memory (MB): peak = 952.547 ; gain = 643.035
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
uFinished IO Insertion : Time (s): cpu = 00:00:42 ; elapsed = 00:00:43 . Memory (MB): peak = 952.547 ; gain = 643.035
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:00:42 ; elapsed = 00:00:44 . Memory (MB): peak = 952.547 ; gain = 643.035
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:00:42 ; elapsed = 00:00:44 . Memory (MB): peak = 952.547 ; gain = 643.035
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:00:42 ; elapsed = 00:00:44 . Memory (MB): peak = 952.547 ; gain = 643.035
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:42 ; elapsed = 00:00:44 . Memory (MB): peak = 952.547 ; gain = 643.035
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:00:42 ; elapsed = 00:00:44 . Memory (MB): peak = 952.547 ; gain = 643.035
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
P
%s
*synth28
$+------+---------------+----------+
2default:defaulth p
x
� 
P
%s
*synth28
$|      |BlackBox name  |Instances |
2default:defaulth p
x
� 
P
%s
*synth28
$+------+---------------+----------+
2default:defaulth p
x
� 
P
%s
*synth28
$|1     |clockGenerator |         1|
2default:defaulth p
x
� 
P
%s
*synth28
$+------+---------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
S
%s*synth2;
'+------+----------------------+------+
2default:defaulth px� 
S
%s*synth2;
'|      |Cell                  |Count |
2default:defaulth px� 
S
%s*synth2;
'+------+----------------------+------+
2default:defaulth px� 
S
%s*synth2;
'|1     |clockGenerator_bbox_0 |     1|
2default:defaulth px� 
S
%s*synth2;
'|2     |BUFG                  |     3|
2default:defaulth px� 
S
%s*synth2;
'|3     |CARRY4                |   662|
2default:defaulth px� 
S
%s*synth2;
'|4     |DSP48E1               |     6|
2default:defaulth px� 
S
%s*synth2;
'|5     |DSP48E1_1             |     3|
2default:defaulth px� 
S
%s*synth2;
'|6     |LUT1                  |   211|
2default:defaulth px� 
S
%s*synth2;
'|7     |LUT2                  |   912|
2default:defaulth px� 
S
%s*synth2;
'|8     |LUT3                  |   610|
2default:defaulth px� 
S
%s*synth2;
'|9     |LUT4                  |   570|
2default:defaulth px� 
S
%s*synth2;
'|10    |LUT5                  |   254|
2default:defaulth px� 
S
%s*synth2;
'|11    |LUT6                  |   729|
2default:defaulth px� 
S
%s*synth2;
'|12    |FDRE                  |   319|
2default:defaulth px� 
S
%s*synth2;
'|13    |FDSE                  |    13|
2default:defaulth px� 
S
%s*synth2;
'|14    |IBUF                  |    19|
2default:defaulth px� 
S
%s*synth2;
'|15    |OBUF                  |    31|
2default:defaulth px� 
S
%s*synth2;
'+------+----------------------+------+
2default:defaulth px� 
E
%s
*synth2-

Report Instance Areas: 
2default:defaulth p
x
� 
o
%s
*synth2W
C+------+--------------------------+-----------------------+------+
2default:defaulth p
x
� 
o
%s
*synth2W
C|      |Instance                  |Module                 |Cells |
2default:defaulth p
x
� 
o
%s
*synth2W
C+------+--------------------------+-----------------------+------+
2default:defaulth p
x
� 
o
%s
*synth2W
C|1     |top                       |                       |  4344|
2default:defaulth p
x
� 
o
%s
*synth2W
C|2     |  Player1                 |Player                 |   279|
2default:defaulth p
x
� 
o
%s
*synth2W
C|3     |    moveClock             |Tick_1                 |    26|
2default:defaulth p
x
� 
o
%s
*synth2W
C|4     |  Player2                 |AI                     |   357|
2default:defaulth p
x
� 
o
%s
*synth2W
C|5     |    aiPlayer              |Player__parameterized1 |   357|
2default:defaulth p
x
� 
o
%s
*synth2W
C|6     |      moveClock           |Tick_0                 |    26|
2default:defaulth p
x
� 
o
%s
*synth2W
C|7     |  ScoreBoard              |ScoreDisplay           |   593|
2default:defaulth p
x
� 
o
%s
*synth2W
C|8     |    clk                   |Tick__parameterized2   |    22|
2default:defaulth p
x
� 
o
%s
*synth2W
C|9     |  VSync                   |VPulse                 |   344|
2default:defaulth p
x
� 
o
%s
*synth2W
C|10    |    HS                    |HPulse                 |   192|
2default:defaulth p
x
� 
o
%s
*synth2W
C|11    |  au                      |AudioDriver            |    36|
2default:defaulth p
x
� 
o
%s
*synth2W
C|12    |    clock                 |Tick__parameterized4   |    20|
2default:defaulth p
x
� 
o
%s
*synth2W
C|13    |  \genClocks[0].genClock  |Tick                   |    26|
2default:defaulth p
x
� 
o
%s
*synth2W
C|14    |  \genClocks[1].genClock  |Tick__parameterized6   |    34|
2default:defaulth p
x
� 
o
%s
*synth2W
C|15    |  randGen                 |Random                 |   519|
2default:defaulth p
x
� 
o
%s
*synth2W
C+------+--------------------------+-----------------------+------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:00:42 ; elapsed = 00:00:44 . Memory (MB): peak = 952.547 ; gain = 643.035
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
s
%s
*synth2[
GSynthesis finished with 0 errors, 0 critical warnings and 66 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
~Synthesis Optimization Runtime : Time (s): cpu = 00:00:35 ; elapsed = 00:00:38 . Memory (MB): peak = 952.547 ; gain = 282.648
2default:defaulth p
x
� 
�
%s
*synth2�
Synthesis Optimization Complete : Time (s): cpu = 00:00:43 ; elapsed = 00:00:44 . Memory (MB): peak = 952.547 ; gain = 643.035
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
g
-Analyzing %s Unisim elements for replacement
17*netlist2
6712default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
g
1Inserted %s IBUFs to IO ports without IO buffers.100*opt2
12default:defaultZ31-140h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
952.5472default:default2
0.0002default:defaultZ17-268h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
1402default:default2
1042default:default2
02default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2"
synth_design: 2default:default2
00:00:442default:default2
00:00:472default:default2
952.5472default:default2
654.5042default:defaultZ17-268h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0012default:default2
952.5472default:default2
0.0002default:defaultZ17-268h px� 
K
"No constraints selected for write.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2f
RC:/Projects/DigitalElectronics/project_1/project_1.runs/synth_1/GameController.dcp2default:defaultZ17-1381h px� 
�
%s4*runtcl2�
rExecuting : report_utilization -file GameController_utilization_synth.rpt -pb GameController_utilization_synth.pb
2default:defaulth px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Thu Nov 28 12:31:43 20192default:defaultZ17-206h px� 


End Record